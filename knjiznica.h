#ifndef KNJIZNICA_H
#define KNJIZNICA_H

typedef struct {
  char author_of_the_book[100];
  char book_title[100];
  int year_of_issue;
  int serial_number;

} book;

void creat_book(book* b);

#endif