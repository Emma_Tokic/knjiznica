C_COMPILER=gcc
C_FLAGS=-g -Wall

knjiznica:main.o knjiznica.o
	$(C_COMPILER) main.o knjiznica.o -o knjiznica.exe

main.o: main.c
	$(C_COMPILER) $(C_FLAGS) -c main.c

knjiznica.o: knjiznica.c
	$(C_COMPILER) $(C_FLAGS) -c knjiznica.c

clear:
	$(RM) *.exe *.o