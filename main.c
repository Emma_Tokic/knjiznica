#include <stdio.h>

#include "knjiznica.h"

const int max_number_of_book = 3;

void main() {
  book library[max_number_of_book];

  for (int i = 0; i < max_number_of_book; i++) {
    creat_book_store(&library[i]);
  }

  for (int i = 0; i < max_number_of_book; i++) {
    printf("-------------------------------------\n");
    printf("Author :%s\n", library[i].author_of_the_book);
    printf("Book titile :%s\n", library[i].book_title);
    printf("Year of issue :%d\n", library[i].year_of_issue);
    printf("Serial number :%d\n", library[i].serial_number);
  }
}