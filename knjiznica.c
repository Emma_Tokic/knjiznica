#include "knjiznica.h"

#include <stdio.h>
#include <time.h>

void create_book(book* b) {
  printf("!----------------------! \n");

  printf("Enter author :\n");
  scanf("%s", (*b).author_of_the_book);
  getchar();

  printf("Enter book titile :\n");
  scanf("%s", (*b).book_title);
  getchar();

  printf("Enter year of issue :\n");
  scanf("%d", &(*b).year_of_issue);
  getchar();

  printf("Enter serial number :\n");
  scanf("%d", &(*b).serial_number);
  getchar();
}